

### how to get started

- extract project 
- docker-compose down && docker-compose build && docker-compose up -d
- docker exec -it app composer install
- docker exec -it app chown -R 777 *
- docker exec -it app cp .env.example .env
- docker-compose exec app php artisan key:generate
- docker-compose exec app php artisan migrate
- docker-compose exec app php artisan db:seed
- docker-compose exec app php artisan passport:install
- can import database using attached sql file
- database contains two tables
- table users contains (name, email, password, type)
- table jobs contains (title, description, status, user_id)
- regualr user (regular@examle.com) >> password is password
- manager user (manager@examle.com) >> password is password
- included also unit tests

### Api Postman Requests
- https://www.postman.com/mo2men

- 127.0.0.1:8080/api/v1/login  >>> method post
- regualr user (regular@examle.com) >> password is password
- manager user (manager@examle.com) >> password is password

- 127.0.0.1:8080/api/v1/jobs  >>> method get 
- it will redirect to all jobs according to logged in user

- 127.0.0.1:8080/api/v1/jobs/{job}  >>> method get 
- it will redirect to specific job according to logged in user

- - 127.0.0.1:8080/api/v1/jobs  >>> method post
- this request is responsible for creating post

- request need passport token so, you can used earlier emails and passwords

-- after creating job, notifications are sent to managers using queue
-- sending notifications are automatically fired in background without restricting htttp request when creating job
-- this action is used using task schedular commands with queue commands 
-- queues jobs and failed jobs are also inserted to database


### Deploy Using Kubernetes
- Pushing Docker Image to Docker Hub
  - docker login -u your_docker_hub_username
  - docker push your_docker_hub_username/ imageName
### Helm configuration 
- docker run --rm -v $(pwd):/app php:cli php /app/artisan key:generate
- echo '/helm/secrets.yml' >> ./.dockerignore && echo '/helm/secrets.yml' >> ./.gitignore
- helm install imageName -f helm/values.yml -f helm/secrets.yml stable/lamp
- kubectl get services -w



