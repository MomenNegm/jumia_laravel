<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\Api\V1\Auth\RegisterApiRequest;
use App\Http\Requests\Api\V1\Auth\LoginApiRequest;

class UserAuthApiController extends Controller
{
     public function register(RegisterApiRequest $request)
    {
    	$request->type = 'manager';
    	$data = $request->all();
        $user = User::create($data);
        $token = $user->createToken('API Token')->accessToken;
        return response([ 'user' => $user, 'token' => $token]);
    }

    public function login(LoginApiRequest $request)
    {
    	$data = $request->only('email', 'password');
        if (!auth()->attempt($data)) {
            return response([
            	'error_message' => 'Incorrect Details. Please try again']);
        }

        $token = auth()->user()->createToken('API Token')->accessToken;
        return response(['user' => auth()->user(), 'token' => $token]);

    }
}
