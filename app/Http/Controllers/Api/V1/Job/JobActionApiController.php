<?php

namespace App\Http\Controllers\Api\V1\Job;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Http\Resources\Api\V1\Job\JobApiResource;
use App\Models\Jobmodel;
use App\Http\Requests\Api\V1\Job\AddJobApiRequest;
use App\Http\Requests\Api\V1\Job\UpdateJobApiRequest;
use App\Models\User;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewJobNotify;
use App\Jobs\NotifyMangersWithJob;

class JobActionApiController extends Controller
{
    // start create job
    public function add_data(AddJobApiRequest $request)
    {
    	$Auth_user = Auth::guard('api')->user();
    	$user_id   = $Auth_user->id;
    	$keyValues = [
    		'title'       => $request->title,
    		'description' => $request->description,
    		'user_id'     => $user_id,
    	];
    	$job      = Jobmodel::create($keyValues);
        if ($job) 
        {    
            // send queue emails here to managers
            $this->queueEmails();

            $data     = new JobApiResource($job);
            $status   = 201;
            $message       = 'Job added successfully';
            $notification  = 'Notifications sent to managers in the background';
            return response()->json([
                'message'      => $message,
                'Notification' => $notification,
                'data'         => $data,
                'status'       => $status,
            ], $status);
        }else{
            return response()->json([
                'message' => 'something went wrong',
                'data'    => null,
                'status'  => 400,
            ], 400);
        }
    }
    // end create job

    public function queueEmails()
    {
        // emails are gonna be fired every minute and restart every 5 minutes
        // start send notification to managers
            $managers = User::where('type', 'manager')->chunk(50, function($data){
                dispatch(new NotifyMangersWithJob($data));
            }); //Retrieving all managers
        // end send notification to managers
    }


    // start update job
    public function update_data(Jobmodel $job, UpdateJobApiRequest $request)
    {
    	$Auth_user = Auth::guard('api')->user();
    	$user_id = $Auth_user->id;
    	$keyValues = [
    		'title'       => $request->title,
    		'description' => $request->description,
    		'user_id'     => $user_id,
    	];
    	$job->update($keyValues);
    	$data    = new JobApiResource($job);
    	$status  = 202;
        $message  = 'Job updated successfully';
    	return response()->json([
            'message' => $message,
    		'data'    => $data,
    		'status'  => $status,
    	], $status);
    }
    // end create job
}
