<?php

namespace App\Http\Controllers\Api\V1\Job;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Api\V1\Job\JobApiResource;
use App\Models\Jobmodel;
use Auth;
use App\Models\User;

class JobApiController extends Controller
{ 
    // start fetch all jobs according to type of user
    public function fetch_data(Request $request)
    {
        $indexes = $this->classification();
    	$data    = JobApiResource::collection($indexes);
    	$status  = 200;
        $count   = $data->count();
    	return response()->json([
            'count' => $count,
    		'data'   => $data,
    		'status' => $status,
    	]);
    }
    // end fetch all jobs according to type of user

   // start check for user type 
    public function classification()
    {
        $Auth_user = Auth::guard('api')->user();
        if ($Auth_user->type == 'manager') {
            $indexes = Jobmodel::select('id', 'title', 'description', 'user_id')->get();
        }else{
            $indexes = Jobmodel::where('user_id', $Auth_user->id)->select('id', 'title', 'description', 'user_id')->get();
        }
        return $indexes;
    }
    // end check for user type 


    // start fetch specific job to type of user
    public function view(Jobmodel $job)
    {
        $Auth_user = Auth::guard('api')->user();
        if ($Auth_user->type == 'regular') {
            if ($Auth_user->id != $job->user_id) {
            $data = null;
            }   
            else{
                $data    = new JobApiResource($job);
            }
        }else{
                $data    = new JobApiResource($job);
        }
        $status  = 200;
        return response()->json([
            'data'   => $data,
            'status' => $status,
        ]);
    }
    // start fetch specific job to type of user
}
