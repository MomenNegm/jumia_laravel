<?php

namespace App\Http\Requests\Api\V1\Job;

use Illuminate\Foundation\Http\FormRequest;

class UpdateJobApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'title' => [
                'required',
                'max:100',
            ],

            'description' => [
                'required',
                'max:255',
            ],

        ];
    }
}
