<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Jobmodel;
use App\Models\User;

class JobmodelFactory extends Factory
{
    
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Jobmodel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        return [
            
            'title'       => $this->faker->sentence,
            'description' => $this->faker->sentence,
            'user_id'     => function() { 
                return User::all()->random()->id;
                },
            'status'      => 1,
            
        ];
    }
}
