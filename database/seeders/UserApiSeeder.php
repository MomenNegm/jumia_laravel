<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserApiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $user_manager = [
        	'name'      => 'manager',
	        'email'     => 'manager@example.com',
	        'password'  => 'password',
	        'type'      => 'manager'
	    ];

	    $user_regular = [
        	'name'      => 'regular',
	        'email'     => 'regular@example.com',
	        'password'  => 'password',
	        'type'      => 'regular'
	    ];

        User::create($user_manager);
        User::create($user_regular);



    }
}
