<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


// start auth section
Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Auth'], function () {

	Route::post('/register', 'UserAuthApiController@register');
    Route::post('/login', 'UserAuthApiController@login');

});
// end auth section

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Job', 'middleware' => ['auth:api']], function () {

	// fetch all jobs according to type of user
	Route::get('jobs', 'JobApiController@fetch_data')->name('jobs.index');
	// fetch specific job 
	Route::get('jobs/{job}', 'JobApiController@view')->name('jobs.view');
	// create new job
	Route::post('jobs', 'JobActionApiController@add_data')->name('jobs.create');
	// update job
	Route::put('jobs/{job}', 'JobActionApiController@update_data')->name('jobs.update');

});
