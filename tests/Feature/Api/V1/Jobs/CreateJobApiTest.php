<?php

namespace Tests\Feature\Api\V1\Jobs;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateJobApiTest extends TestCase
{
    // use RefreshDatabase;
      // use DatabaseMigrations;
     // use  DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_car_made()
    {
        $this->withoutExceptionHandling();
        $user       = \App\Models\User::factory()->create();
        $this->actingAs($user, 'api');
        $faker      = \Faker\Factory::create();
        $form_data  = [
            'title'       => 'some title',
            'description' => 'some description',
            'user_id'     => $user->id,
            'status'      => 1,
        ];

        $reponse = $this->json('POST', route('api.jobs.create'), $form_data, [
            'Accept' => 'application/json',
        ]);

        $reponse->assertStatus(201);
        
    }
}
