<?php

namespace Tests\Feature\Api\V1\Jobs;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FetchAllApiTest extends TestCase
{
    // use DatabaseMigrations;
   // use DatabaseTransactions;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware();
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_caryears()
    {
        $this->withoutExceptionHandling();
        $jobs   = \App\Models\Jobmodel::factory()->create();
        $user   = \App\Models\User::factory()->create();
        $this->actingAs($user, 'api');
        
        $reponse = $this->json('GET', route('api.jobs.index'), [
            'Accept'        => 'application/json',
            //'Authorization' => 'Bearer '. $auth_token,
        ]);
        $reponse->assertJsonStructure([
            'data' => [
                '*' => [
                    "id",           
                    "title",        
                    "description",
                    "userName",     
                    "userType",     
                    "created_at", 
                ] // end *
            ], // end data 
            'count',
        ]) // end structure
            ->assertStatus(200);
    }
}
